package org.nrg.xnat.client.rest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;


public class PostFileWS extends WSTool {
    protected static final String FILE_FLAG       = "f";
    protected static final String DEST_FLAG       = "dest";
    protected static final String SRC_FLAG       = "src";
    
    
    public PostFileWS() {
        super();
        this.initialArguments.add(SRC_FLAG);
        this.initialArguments.add(DEST_FLAG);
    }

    public void process() {
        // OUTPUT DIRECTORY
        String src = null;
        String dest = null;
        
        File srcF=null;

        if ((!arguments.containsKey(DEST_FLAG))) {
            displayHelp();
            System.exit(4);
        }

        try {
            
        	
        	if(arguments.containsKey(SRC_FLAG))src = (String)arguments.get(SRC_FLAG);
        	if(arguments.containsKey(DEST_FLAG)){
        		dest = (String)arguments.get(DEST_FLAG);
        	}
            
        	if(src!=null){
        		if(!(src.equals("")|| src.equals("\"\""))){
            		srcF = new File(src);
                    if(!srcF.exists()){
                        throw new FileNotFoundException(src);
                    }
        		}
        	}
                        
            String service_session = createServiceSession();
            try {
                // REQUEST SESSION ID
                // call.setProperty(Call.CHARACTER_SET_ENCODING,"UTF-8");

                    execute(host, service_session, srcF, dest, quiet);
            } catch (AxisFault ex2) {
                String fault = ex2.getFaultString();
                if (fault == null) {
                    error(33, "Web Service Exception: " + host + "\n" + ex2.getMessage(), ex2);
                } else if (fault.indexOf("PasswordAuthenticationException") != -1) {
                    error(99, "Invalid Password.", ex2);
                } else if (fault.indexOf("FailedLoginException") != -1) {
                    error(98, "Failed Login. Review username and password.", ex2);
                } else if (fault.indexOf("UserNotFoundException") != -1) {
                    error(97, "Failed Login. Review username and password.", ex2);
                } else if (fault.indexOf("EnabledException") != -1) {
                    error(96, "Failed Login. Account disabled.", ex2);
                } else {
                    error(32, "Web Service Exception @ " + host + "\n" + fault, ex2);
                }
            } catch (RemoteException ex) {
                error(33, "Web Service Exception: " + host + "\n" + ex.getMessage(), ex);
            } catch (MalformedURLException e) {
                error(12, "Web Service Exception: " + host + "\n" + e.getMessage(), e);
            } catch (Throwable e) {
                error(13, "Web Service Exception: " + host + "\n" + e.getMessage(), e);
            }
            closeServiceSession(service_session);
        } catch (MalformedURLException e) {
            error(12, "Web Service Exception: " + host + "\n" + e.getMessage(), e);
        } catch (AxisFault ex2) {
            String fault = ex2.getFaultString();
            if (fault == null) {
                error(33, "Web Service Exception: " + host + "\n" + ex2.getMessage(), ex2);
            } else if (fault.indexOf("PasswordAuthenticationException") != -1) {
                error(99, "Invalid Password.", ex2);
            } else if (fault.indexOf("FailedLoginException") != -1) {
                error(98, "Failed Login. Review username and password.", ex2);
            } else if (fault.indexOf("UserNotFoundException") != -1) {
                error(97, "Failed Login. Review username and password.", ex2);
            } else if (fault.indexOf("EnabledException") != -1) {
                error(96, "Failed Login. Account disabled.", ex2);
            } else {
                error(32, "Web Service Exception @ " + host + "\n" + fault, ex2);
            }
        } catch (RemoteException ex) {
            error(33, "Web Service Exception: " + host + "\n" + ex.getMessage(), ex);
        } catch (ServiceException ex) {
            error(11, "Web Service Exception: " + host + "\n" + ex.getMessage(), ex);
        } catch (FileNotFoundException ex) {
            error(34, ex.getMessage(), ex);
        }
    }

    /**
     * @param host
     *            Host of the server i.e. 'http://localhost:8080/xnat'
     * @param service_session
     *            create from 'createServiceSession()'
     * @param file
     *            file to upload
     * @param quiet
     *            limits system output.
     * @throws FileNotFoundException
     * @throws MalformedURLException
     * @throws IOException
     */
    public void execute(String host, String service_session,File srcF,String dest, boolean quiet)
            throws FileNotFoundException, MalformedURLException, IOException {
        long startTime = System.currentTimeMillis();

    	String urlString=null;
        if(host.endsWith("/")&& dest.startsWith("/")){
        	urlString = host + dest.substring(1);
        	
        }else if (!host.endsWith("/")&& !dest.startsWith("/")){
        	if(!dest.startsWith("http"))
        		urlString=host +"/" + dest;
        	else
        		urlString=dest;
        }else{
        	urlString = host + dest;
        }

        HttpClient client = new HttpClient();
    	PostMethod filePost = new PostMethod(urlString);
        filePost.addRequestHeader("Cookie", "JSESSIONID="+service_session);
        client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        if(srcF!=null)
        {
	        Part[] parts={new FilePart("img1",srcF)};
	        filePost.setRequestEntity(new MultipartRequestEntity(parts,filePost.getParams()));
        }
        int statusCode= client.executeMethod(filePost);
        
        
        if(statusCode==200)
            System.exit(0);
        else
        	System.exit(statusCode);
        
    }
    
    public void loadHelpText(){
        super.loadHelpText();
        helpText.put(SRC_FLAG, "Local file to upload URL.");
        helpText.put(DEST_FLAG, "Destination URL.");
    }

    public void displayHelp() {
        System.out.println("\nPost File Web Service\n");
        displayCommonHelp();
        printHelpLine(DEST_FLAG);
        printHelpLine(SRC_FLAG);
    }

    public static void main(String[] args) {
        PostFileWS arcGet = new PostFileWS();
        arcGet.perform(args);
    }
    
}