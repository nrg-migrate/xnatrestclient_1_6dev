package org.nrg.xnat.client.rest;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;
import org.apache.axis.client.Call;

//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Created on Sep 11, 2006
 *
 */

/**
 * @author timo
 *
 */
public class ArcSimWS  extends WSTool{
    
    public void process(){		
	    int _return = 0;
	    try {
	        URL url = new URL(host + "axis/GetIdentifiers.jws");
            
            String service_session=this.createServiceSession();
            
            Call call = createCall(service_session);
            call.setTargetEndpointAddress(url);
            
            call.setOperationName("search");
            
            String field ="xnat:mrSessionData.ID";
            
            String comparison = "LIKE";
            
            String value = (String)arguments.get("s");
            if (value==null)
            {
                System.out.println("ERROR CODE 28: Missing partial session id.");
                displayHelp();
                System.exit(28);
            }
            
            
            String dataType = "xnat:mrSessionData";

            Object[] params=new Object[]{service_session,field,comparison,value,dataType};

            Object[] o = (Object[])call.invoke(params);
    	    
    	    String outFile = (String)arguments.get("o");
    	    
    	    
            for (int i =0;i<o.length;i++)
            {
                Object id = (Object)o[i];
                if (outFile==null)
                    System.out.println(id);
                else
                    outputToFile((String)id,outFile,true);
            }
            
            if (outFile !=null && (new File(outFile)).exists())
            {
                System.out.println("\nCreated file: " + outFile);
            }
            
        }catch(AxisFault ex2)
        {
            String fault = ex2.getFaultString();
            if (fault==null){
                System.out.println("ERROR CODE 33: \nWeb Service Exception: " + host + "\n" + ex2.getMessage());
                logError(ex2);
                System.exit(33);
            }else if (fault.indexOf("PasswordAuthenticationException")!=-1)
            {
      		    System.out.println("ERROR CODE 99: \nInvalid Password.");
                logError(ex2);
            	System.exit(99);
            }else if (fault.indexOf("FailedLoginException")!=-1)
            {
      		    System.out.println("ERROR CODE 98: \nFailed Login. Review username and password.");
                logError(ex2);
            	System.exit(98);
            }else if (fault.indexOf("UserNotFoundException")!=-1)
            {
      		    System.out.println("ERROR CODE 97: \nFailed Login. Review username and password.");
                logError(ex2);
            	System.exit(98);
            }else if (fault.indexOf("EnabledException")!=-1)
            {
      		    System.out.println("ERROR CODE 96: \nFailed Login. Account disabled.");
                logError(ex2);
            	System.exit(98);
            }else{
      		    System.out.println("ERROR CODE 32: \nWeb Service Exception @ " + host + "\n" + fault);
                logError(ex2);
            	System.exit(32);
            }
        }catch (RemoteException ex) {
		    System.out.println("ERROR CODE 33: " + ex.getMessage());
            logError(ex);
            _return= 33;
        } catch (ServiceException ex) {
            System.out.println("ERROR CODE: 11\nWeb Service Exception: " + host + "\n" + ex.getMessage());
            logError(ex);
			_return= 11;
        }catch (MalformedURLException e) {
            System.out.println("ERROR CODE: 12\nUnknown URL: " + host);
            logError(e);
            System.exit(12);
        }
		System.exit(_return);
    }
    
    public ArcSimWS(){
        super();
    }
    
    public void displayHelp(){
        System.out.println("\nArc-Sim Web Service\n");
        System.out.println("");
        System.out.println("Parameters:");
        System.out.println("-u          USERNAME");
        System.out.println("-p          PASSWORD");
        System.out.println("-host       URL to XNAT based website.  (i.e. http://localhost/xnat)");
        System.out.println("-h          Print help.");
        System.out.println("-s          Partial session id.");
        System.out.println("-o          Name of file to output to.");
    }
    
    
    public static void main(String[] args) {
        
        ArcSimWS arcSim = new ArcSimWS();
        arcSim.perform(args);
    }
    
}