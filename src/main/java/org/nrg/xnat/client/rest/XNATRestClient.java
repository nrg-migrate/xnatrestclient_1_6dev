package org.nrg.xnat.client.rest;//Copyright 2011 Washington University School of Medicine All Rights Reserved
/*
 * Modified on Dec 10th, 2011
 *
 */
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.text.NumberFormat;

import org.apache.axis.AxisFault;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.EntityEnclosingMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.util.URIUtil;


import java.util.Arrays;


public class XNATRestClient extends WSTool {
    protected static final String METHOD_FLAG       = "m";
    protected static final String DEST_FLAG       = "remote";
    protected static final String SRC_FLAG       = "local";
    
    private static final int BYTE_BUFFER_LENGTH = 256;
    
    public XNATRestClient() {
        super();
    }

    public void process() {
        // OUTPUT DIRECTORY
        String src = null;
        String dest = null;
        String method =null;
        
        File srcF=null;

        if ((!arguments.containsKey(METHOD_FLAG))) {
        	System.out.println("Missing -m tag");
            displayHelp();
            System.exit(4);
        }
        
        method=(String)arguments.get(METHOD_FLAG);
        if(method.equalsIgnoreCase("GET")){
        	method="GET";
        }else if(method.equalsIgnoreCase("POST")){
        	method="POST";
        }else if(method.equalsIgnoreCase("PUT")){
        	method="PUT";
        }else if(method.equalsIgnoreCase("DELETE")){
        	method="DELETE";
        }else{
        	System.out.println("Missing properly formed -m tag (GET, POST, PUT, DELETE)");
            displayHelp();
            System.exit(4);
        }

        if ((!arguments.containsKey(DEST_FLAG))) {
        	System.out.println("Missing -dest tag");
            displayHelp();
            System.exit(4);
        }

        try {
            
        	
        	if(arguments.containsKey(SRC_FLAG))src = (String)arguments.get(SRC_FLAG);
        	if(arguments.containsKey(DEST_FLAG)){
        		dest = (String)arguments.get(DEST_FLAG);
        	}
            
        	if(src!=null){
        		if(!(src.equals("")|| src.equals("\"\""))){
            		srcF = new File(src);
                    if(!srcF.exists() && !(method.equals("GET")|| method.equals("DELETE"))){
                        throw new FileNotFoundException(src);
                    }
        		}
        	}
                        
            try {
                // REQUEST SESSION ID
                // call.setProperty(Call.CHARACTER_SET_ENCODING,"UTF-8");

                    execute(srcF, dest, quiet,method);
            } catch (AxisFault ex2) {
                String fault = ex2.getFaultString();
                if (fault == null) {
                    error(33, "Web Service Exception: " + host + "\n" + ex2.getMessage(), ex2);
                } else if (fault.indexOf("PasswordAuthenticationException") != -1) {
                    error(99, "Invalid Password.", ex2);
                } else if (fault.indexOf("FailedLoginException") != -1) {
                    error(98, "Failed Login. Review username and password.", ex2);
                } else if (fault.indexOf("UserNotFoundException") != -1) {
                    error(97, "Failed Login. Review username and password.", ex2);
                } else if (fault.indexOf("EnabledException") != -1) {
                    error(96, "Failed Login. Account disabled.", ex2);
                } else {
                    error(32, "Web Service Exception @ " + host + "\n" + fault, ex2);
                }
            } catch (RemoteException ex) {
                error(33, "Web Service Exception: " + host + "\n" + ex.getMessage(), ex);
            } catch (MalformedURLException e) {
                error(12, "Web Service Exception: " + host + "\n" + e.getMessage(), e);
            } catch (Throwable e) {
                error(13, "Web Service Exception: " + host + "\n" + e.getMessage(), e);
            }
        } catch (FileNotFoundException ex) {
            error(34, ex.getMessage(), ex);
        }
    }

    /**
     * @param srcF Local file to upload to the server (can be null)
     * @param dest Remote url 
     * @param quiet whether or not to suppress notifications
     * @param method GET, PUT, POST, or DELETE
     * @throws FileNotFoundException
     * @throws MalformedURLException
     * @throws IOException
     */
    public void execute(File srcF,String dest, boolean quiet, String method)
            throws FileNotFoundException, MalformedURLException, IOException {
        String urlString=null;
        
        //build remote url using supplied host
        if(host.endsWith("/")&& dest.startsWith("/")){
        	urlString = host + dest.substring(1);
        	
        }else if (!host.endsWith("/")&& !dest.startsWith("/")){
        	if(!dest.startsWith("http"))
        		urlString=host +"/" + dest;
        	else
        		urlString=dest;
        }else{
        	urlString = host + dest;
        }
        
        //encode any query sring parameters
        if(urlString.indexOf("?")>-1){
        	String query=urlString.substring(urlString.indexOf("?")+1);
        	urlString=urlString.substring(0,urlString.indexOf("?")+1);
        	
        	int count=0;
        	while(query.indexOf("&")>-1){
        		String pair = query.substring(0,query.indexOf("&"));
        		query=query.substring(query.indexOf("&")+1);
        		String key=pair.substring(0,pair.lastIndexOf("="));
        		String value = pair.substring(pair.lastIndexOf("=")+1);
        		
        		if(count++>0)urlString+="&";
        		
        		urlString+=URIUtil.encodeWithinQuery(key);
        		urlString+="=";
        		urlString+=URIUtil.encodeWithinQuery(value);
        	}
        	
        	if(query.indexOf("=")>-1){
        		String key=query.substring(0,query.lastIndexOf("="));
        		String value = query.substring(query.lastIndexOf("=")+1);
        		
        		if(count++>0)urlString+="&";
        		
        		urlString+=URIUtil.encodeWithinQuery(key);
        		urlString+="=";
        		urlString+=URIUtil.encodeWithinQuery(value);
        	}else{
        		urlString+=query;
        	}
        }

        //build http method
        HttpClient client = new HttpClient();
        HttpMethodBase filePost=null;
        if(method.equals("POST")){
       	 	filePost= new PostMethod(urlString);
        }else if(method.equals("GET")){
       	 	filePost= new GetMethod(urlString);
        }else if(method.equals("PUT")){
       	 	filePost= new PutMethod(urlString);
        }else if(method.equals("DELETE")){
       	 	filePost= new DeleteMethod(urlString);
        }
        
        //if user supplied a http-session-id, attach it as a cookie, else use the basic authorization header
        if (userSessionID==null){
        	filePost.addRequestHeader("Authorization", "Basic "+ new Base64().encode((this.userName + ":" +this.password).getBytes(), false));
        }else{
            externalSessionID = true;
            filePost.addRequestHeader("Cookie", "JSESSIONID="+userSessionID);
        }
        
        //extend timeout
        client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        
        if(srcF!=null && (filePost instanceof EntityEnclosingMethod))
        {
        	//supplied local file and requested a PUT or POST
	        Part[] parts={new FilePart("img1",srcF)};
	        ((EntityEnclosingMethod)filePost).setRequestEntity(new MultipartRequestEntity(parts,filePost.getParams()));
        }else if(srcF!=null){
        	//supplied local flie for a GET
        	if(srcF.exists()){
        		error(77, "File already Exists " + srcF.getAbsolutePath(),null);
        	}
        }
        
        //execute query
        int statusCode= client.executeMethod(filePost);

        //write output
        InputStream is = filePost.getResponseBodyAsStream();
        
        OutputStream os = null;
        

        byte[] buff = new byte[BYTE_BUFFER_LENGTH];
        int bytesRead;
        int loaded = 0;
        
        if(srcF==null || (filePost instanceof EntityEnclosingMethod) || (statusCode>=200 && statusCode<300)){
        	//write to system.out
            os = System.out;
        }else{
        	//write to specified local file
            if (srcF.getParentFile()!=null && !srcF.getParentFile().exists()){
            	srcF.getParentFile().mkdirs();
            }
            os = new FileOutputStream(srcF);
            os = new BufferedOutputStream(os);

        }

        java.text.NumberFormat nf = NumberFormat.getInstance();
        while (-1 != (bytesRead = is.read(buff, 0, buff.length))) {
            os.write(buff, 0, bytesRead);
            os.flush();
            loaded = loaded + bytesRead;
        }

        is.close();
        os.flush();
        if(!(srcF==null || (filePost instanceof EntityEnclosingMethod) || (statusCode>=200 || statusCode<300))){
        	os.close();
        	System.out.println();
        }
        
        if(statusCode>=200 && statusCode<300)
            System.exit(0);
        else
        	System.exit(statusCode);
        
    }
    
    public void loadHelpText(){
        super.loadHelpText();
        helpText.put(METHOD_FLAG, "HTTP Method (GET,POST,PUT,DELETE)");
        helpText.put(SRC_FLAG, "Local file to upload URL.");
        helpText.put(DEST_FLAG, "Remote URL to GET or POST to.");
    }

    public void displayHelp() {
        System.out.println("\nXNAT REST Client Web Service\n");
        displayCommonHelp();
        printHelpLine(DEST_FLAG);
        printHelpLine(SRC_FLAG);
        printHelpLine(METHOD_FLAG);
    }

    public static void main(String[] args) {
    	XNATRestClient arcGet = new XNATRestClient();
        arcGet.perform(args);
    }
    
    
    /**
     * Copyright 2005-2008 Noelios Technologies.
     * 
     * The contents of this file are subject to the terms of the following open
     * source licenses: LGPL 3.0 or LGPL 2.1 or CDDL 1.0 (the "Licenses"). You can
     * select the license that you prefer but you may not use this file except in
     * compliance with one of these Licenses.
     * 
     * You can obtain a copy of the LGPL 3.0 license at
     * http://www.gnu.org/licenses/lgpl-3.0.html
     * 
     * You can obtain a copy of the LGPL 2.1 license at
     * http://www.gnu.org/licenses/lgpl-2.1.html
     * 
     * You can obtain a copy of the CDDL 1.0 license at
     * http://www.sun.com/cddl/cddl.html
     * 
     * See the Licenses for the specific language governing permissions and
     * limitations under the Licenses.
     * 
     * Alternatively, you can obtain a royaltee free commercial license with less
     * limitations, transferable or non-transferable, directly at
     * http://www.noelios.com/products/restlet-engine
     * 
     * Restlet is a registered trademark of Noelios Technologies.
     */
    
    /**
     * Minimal but fast Base64 codec.
     * 
     * @author Ray Waldin (ray@waldin.net)
     */

    /**
     * decoding involves replacing each character with the character's value, or
     * position, from the above alphabet, and this table makes such lookups
     * quick and easy. Couldn't help myself with the corny name :)
     */
    private static final byte[] DECODER_RING = new byte[128];
    /** alphabet used for encoding bytes into base64 */
    private static final char[] BASE64_DIGITS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
            .toCharArray();

    static {
        Arrays.fill(DECODER_RING, (byte) -1);
        int i = 0;
        for (final char c : BASE64_DIGITS) {
            DECODER_RING[c] = (byte) i++;
        }
        DECODER_RING['='] = 0;
    }
    
    public class Base64 {
        private final int byteAt(byte[] data, int block, int off) {
            return unsign(data[(block * 3) + off]);
        }
        
        /**
         * Decodes a base64 string into bytes. Newline characters found at block
         * boundaries will be ignored.
         * 
         * @param encodedString
         *            The string to decode.
         * @return The decoded byte array.
         */
        public byte[] decode(String encodedString) {
            final char[] chars = encodedString.toCharArray();

            // prepare to ignore newline chars
            int newlineCount = 0;
            for (final char c : chars) {
                switch (c) {
                case '\r':
                case '\n':
                    newlineCount++;
                    break;

                default:
                }
            }

            final int len = chars.length - newlineCount;
            int numBytes = ((len + 3) / 4) * 3;

            // fix up length relative to padding
            if (len > 1) {
                if (chars[chars.length - 2] == '=') {
                    numBytes -= 2;
                } else if (chars[chars.length - 1] == '=') {
                    numBytes--;
                }
            }

            final byte[] result = new byte[numBytes];
            int newlineOffset = 0;

            // decode each block of 4 chars into 3 bytes
            for (int i = 0; i < (len + 3) / 4; ++i) {
                int charOffset = newlineOffset + (i * 4);

                final char c1 = chars[charOffset++];
                final char c2 = chars[charOffset++];
                final char c3 = chars[charOffset++];
                final char c4 = chars[charOffset++];

                if (!(validChar(c1) && validChar(c2) && validChar(c3) && validChar(c4))) {
                    throw new IllegalArgumentException(
                            "Invalid Base64 character in block: '" + c1 + c2 + c3
                                    + c4 + "'");
                }

                // pack
                final int x = DECODER_RING[c1] << 18 | DECODER_RING[c2] << 12
                        | (c3 == '=' ? 0 : DECODER_RING[c3] << 6)
                        | (c4 == '=' ? 0 : DECODER_RING[c4]);

                // unpack
                int byteOffset = i * 3;
                result[byteOffset++] = (byte) (x >> 16);
                if (c3 != '=') {
                    result[byteOffset++] = (byte) ((x >> 8) & 0xFF);
                    if (c4 != '=') {
                        result[byteOffset++] = (byte) (x & 0xFF);
                    }
                }

                // skip newlines after block
                outer: while (chars.length > charOffset) {
                    switch (chars[charOffset++]) {
                    case '\r':
                    case '\n':
                        newlineOffset++;
                        break;

                    default:
                        break outer;
                    }
                }
            }
            return result;
        }

        /**
         * Encodes an entire byte array into a Base64 string, with optional newlines
         * after every 76 characters.
         * 
         * @param bytes
         *            The byte array to encode.
         * @param newlines
         *            Indicates whether or not newlines are desired.
         * @return The encoded string.
         */
        public String encode(byte[] bytes, boolean newlines) {
            return encode(bytes, 0, bytes.length, newlines);
        }

        /**
         * Encodes specified bytes into a Base64 string, with optional newlines
         * after every 76 characters.
         * 
         * @param bytes
         *            The byte array to encode.
         * @param off
         *            The starting offset.
         * @param len
         *            The number of bytes to encode.
         * @param newlines
         *            Indicates whether or not newlines are desired.
         * 
         * @return The encoded string.
         */
        public String encode(byte[] bytes, int off, int len, boolean newlines) {
            final char[] output = new char[(((len + 2) / 3) * 4)
                    + (newlines ? len / 43 : 0)];
            int pos = 0;

            // encode each block of 3 bytes into 4 chars
            for (int i = 0; i < (len + 2) / 3; ++i) {

                int pad = 0;
                if (len + 1 < (i + 1) * 3) {
                    // two trailing '='s
                    pad = 2;
                } else if (len < (i + 1) * 3) {
                    // one trailing '='
                    pad = 1;
                }

                // pack
                final int x = (byteAt(bytes, i, off) << 16)
                        | (pad > 1 ? 0 : (byteAt(bytes, i, off + 1) << 8))
                        | (pad > 0 ? 0 : (byteAt(bytes, i, off + 2)));

                // unpack
                output[pos++] = BASE64_DIGITS[x >> 18];
                output[pos++] = BASE64_DIGITS[(x >> 12) & 0x3F];
                output[pos++] = pad > 1 ? '=' : BASE64_DIGITS[(x >> 6) & 0x3F];
                output[pos++] = pad > 0 ? '=' : BASE64_DIGITS[x & 0x3F];

                if (newlines && ((i + 1) % 19 == 0)) {
                    output[pos++] = '\n';
                }
            }
            return new String(output, 0, pos);
        }

        private final int unsign(byte b) {
            return b < 0 ? b + 256 : b;
        }

        private final boolean validChar(char c) {
            return (c < 128) && (DECODER_RING[c] != -1);
        }
    }
}
