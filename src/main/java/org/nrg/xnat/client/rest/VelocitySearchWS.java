package org.nrg.xnat.client.rest;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;
import org.apache.axis.client.Call;

//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Created on Sep 11, 2006
 *
 */

/**
 * @author timo
 *
 */
public class VelocitySearchWS extends WSTool{
	
    public VelocitySearchWS(){
        super();
    }
    
    public void process(){
        //OUTPUT DIRECTORY
        String dir = null;
        Object d = arguments.get("dir");
        if (d==null)
        {
            dir = "." + File.separator;
        }else{
            if (d instanceof ArrayList)
            {
                dir = (String)((ArrayList)d).get(((ArrayList)d).size()-1);
                System.out.println("Exporting to " + dir +".\n Ignoring other -dir tags.");
            }else{
                dir = (String)d;
            }
            if (!dir.endsWith(File.separator))
            {
                dir +=File.separator;
            }
        }

		File f = new File(dir);
		if (!f.exists())
		{
		    f.mkdir();
		}
		
	    int _return = 0;
	    try {
            
//      REQUEST SESSION ID
            String service_session = this.createServiceSession();
	        
            
	        URL url = new URL(host + "axis/GetIdentifiers.jws");
            Call call = this.createCall(service_session);
            call.setTargetEndpointAddress(url);
            
            call.setOperationName("search");
            
            String user = userName;
            String pass = password;
            String field =(String)arguments.get("field");
            if (field==null)
            {
                System.out.println("ERROR CODE 27: Missing search field.");
                displayHelp();
                System.exit(27);
            }
            
            String comparison = (String)arguments.get("c");
            if (comparison==null){
                comparison ="=";
            }
            
            String value = (String)arguments.get("v");
            if (value==null)
            {
                System.out.println("ERROR CODE 28: Missing search value.");
                displayHelp();
                System.exit(28);
            }
            
            
            String dataType = (String)arguments.get("dataType");
            
            if (dataType==null)
            {
                int index = -1;
                index = field.indexOf('.');
                if (index==-1){
                    index = field.indexOf('/');
                }else{
                    int other = field.indexOf('/');
                    if (other ==-1){
                        
                    }else if (other < index){
                        index=other;
                    }
                }
                
                if (index==-1)
                {
                    System.out.println("ERROR CODE 41: Invalid field format.");
                    displayHelp();
                    System.exit(41);
                }
                dataType = field.substring(0,index);
            }
            
            Object[] params=new Object[]{field,comparison,value,dataType};

    	    if (!quiet)System.out.println("Requesting matching IDs...");
    	    long startTime = Calendar.getInstance().getTimeInMillis();
            Object[] o = (Object[])call.invoke(params);
    	    long duration = Calendar.getInstance().getTimeInMillis() - startTime;
    	    if (!quiet)System.out.println("Response Received (" + duration + " ms)");

    	    if (!quiet)System.out.println(o.length + " Matching Item(s) Found.");
    	    
            for (int i =0;i<o.length;i++)
            {
                Object id = (Object)o[i];
                            	
            	call = createCall(service_session);
                url = new URL(host + "axis/VelocitySearch.jws");
                call.setTargetEndpointAddress(url);
                call.setOperationName("search");
                params = new Object[]{service_session,"xnat:mrSessionData.ID","=",id,"xnat:mrSessionData"};
                

        	    if (!quiet)System.out.println("Sending Request...");
        	    startTime = Calendar.getInstance().getTimeInMillis();
                String s = (String)call.invoke(params);
        	     duration = Calendar.getInstance().getTimeInMillis() - startTime;
        	    if (!quiet)System.out.println("Response Received (" + duration + " ms)\n\n");
        	    if (!quiet)System.out.println(s);
            }
            
            closeServiceSession(service_session);
        }catch(AxisFault ex2)
        {
            String fault = ex2.getFaultString();
            if (fault==null){
                System.out.println("ERROR CODE 33: \nWeb Service Exception: " + host + "\n" + ex2.getMessage());
            }else if (fault.indexOf("PasswordAuthenticationException")!=-1)
            {
      		    System.out.println("ERROR CODE 99: \nInvalid Password.");
            	System.exit(99);
            }else if (fault.indexOf("FailedLoginException")!=-1)
            {
      		    System.out.println("ERROR CODE 98: \nFailed Login. Review username and password.");
            	System.exit(98);
            }else if (fault.indexOf("UserNotFoundException")!=-1)
            {
      		    System.out.println("ERROR CODE 97: \nFailed Login. Review username and password.");
            	System.exit(98);
            }else if (fault.indexOf("EnabledException")!=-1)
            {
      		    System.out.println("ERROR CODE 96: \nFailed Login. Account disabled.");
            	System.exit(98);
            }else{
      		    System.out.println("ERROR CODE 32: \nWeb Service Exception @ " + host + "\n" + fault);
            	System.exit(32);
            }
        }catch (RemoteException ex) {
		    System.out.println("ERROR CODE 33: " + ex.getMessage());
            _return= 33;
        } catch (ServiceException ex) {
            System.out.println("ERROR CODE: 11\nWeb Service Exception: " + host + "\n" + ex.getMessage());
			_return= 11;
        }catch (MalformedURLException e) {
            System.out.println("ERROR CODE: 12\nUnknown URL: " + host);
            System.exit(12);
        }
		System.exit(_return);
    }
    
    public void displayHelp(){
        System.out.println("\nText Search Web Service\n");
        System.out.println("");
        System.out.println("Parameters:");
        System.out.println("-userName          USERNAME");
        System.out.println("-password          PASSWORD");
        System.out.println("-host       URL to XNAT based website.  (i.e. http://localhost/xnat)");
        System.out.println("-field      (REQUIRED) field to search on - Must be specified using dot syntax from the parent element. (i.e. ClinicalAssessment.neuro.CDR.memory");
        System.out.println("-c          (i.e. '=','<','<=','>','>=', or 'LIKE').");
        System.out.println("-v          (REQUIRED) value to search for. ");
        System.out.println("-dataType   Root level data type to return.");
        System.out.println("-quiet      Minimize system output.");
        System.out.println("-h          Print help.");
    }
    
    
    public static void main(String[] args) {
        
        VelocitySearchWS velocitySearch = new VelocitySearchWS();
        velocitySearch.perform(args);
    }
    
}