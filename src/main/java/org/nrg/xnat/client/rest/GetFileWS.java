package org.nrg.xnat.client.rest;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.RemoteException;
import java.text.NumberFormat;
import java.util.zip.GZIPInputStream;

import javax.xml.rpc.ServiceException;


//Copyright 2007 Washington University School of Medicine All Rights Reserved
/*
 * Created on Sep 28, 2007
 *
 */

public class GetFileWS extends WSTool {
    protected static final String SRC_FLAG       = "src";
    protected static final String DEST_FLAG       = "dest";

    
    public void loadHelpText(){
        super.loadHelpText();
        helpText.put(USER_SESSION_FLAG, "User Session ID");
        helpText.put(SRC_FLAG, "Source File (REMOTE_PATH)");
        helpText.put(DEST_FLAG, "Destinating File (LOCAL_PATH)");
    }

    public void displayHelp() {
        System.out.println("\nClose User Web Service Session\n");
        displayCommonHelp();
        printHelpLine(SRC_FLAG);
        printHelpLine(DEST_FLAG);
        printHelpLine(DECOMPRESS_FLAG);
    }

    public static void main(String[] args) {
        GetFileWS serv = new GetFileWS();
        serv.perform(args);
    }

    public GetFileWS() {
        super();
    }

    public void process() {

        if (arguments.get(SRC_FLAG) == null) {
            displayHelp();
            System.exit(5);
        }

        if (arguments.get(DEST_FLAG) == null) {
            displayHelp();
            System.exit(6);
        }
        
        try {
            final String service_session = this.createServiceSession();
            final String src=(String)arguments.get(SRC_FLAG);
            String dest=(String)arguments.get(DEST_FLAG);

        	String urlString=null;
            if(host.endsWith("/")&& src.startsWith("/")){
            	urlString = host + src.substring(1);
            	
            }else if (!host.endsWith("/")&& !src.startsWith("/")){
            	if(!src.startsWith("http"))
            		urlString=host +"/" + src;
            	else
            		urlString=src;
            }else{
            	urlString = host + src;
            }
            
            try {
                URLConnection url = new URL(urlString).openConnection();
                url.setRequestProperty("Cookie", "JSESSIONID="+service_session);
                // Use Buffered Stream for reading/writing.
                InputStream bis = null;
                OutputStream os = null;
    
                
                String decompressS = (String) arguments.get(DECOMPRESS_FLAG);
                boolean decompress = false;
                if (decompressS != null) {
                    if (decompressS.equalsIgnoreCase("true")) {
                        if (dest.endsWith(".gz")){
                            dest= dest.substring(0,dest.length()-3);
                            decompress = true;
                        }
                    }
                }
            
                File outFile = new File(dest);
                
                if (outFile.getParentFile()!=null && !outFile.getParentFile().exists()){
                    outFile.getParentFile().mkdirs();
                }
                
                os = new FileOutputStream(outFile);

                try {
                    bis = url.getInputStream();
                } catch (FileNotFoundException e) {
                    error(39, "File not found on server.  Please review the -src parameter", e);
                }
                
                if (decompress)
                    bis = new GZIPInputStream(bis);
                
                os = new BufferedOutputStream(os);
                

                byte[] buff = new byte[256];
                int bytesRead;
                int loaded = 0;

                java.text.NumberFormat nf = NumberFormat.getInstance();
                while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                    os.write(buff, 0, bytesRead);
                    os.flush();
                    loaded = loaded + bytesRead;
                }

                bis.close();
                os.flush();
                os.close();
                
                
                if(!quiet)System.out.println(outFile.getAbsolutePath());
            } catch (FileNotFoundException e) {
                error(37, e.getMessage(), e);
            } catch (IOException e) {
                error(38, e.getMessage(), e);
            }
        } catch (MalformedURLException e) {
            error(12, "Web Service Exception: " + host + "\n" + e.getMessage(), e);
        } catch (RemoteException ex) {
            error(33, "Web Service Exception: " + host + "\n" + ex.getMessage(), ex);
        } catch (ServiceException ex) {
            error(11, "Web Service Exception: " + host + "\n" + ex.getMessage(), ex);
        }
    }
}
