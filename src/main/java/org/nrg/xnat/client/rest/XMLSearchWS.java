package org.nrg.xnat.client.rest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Created on Aug 28, 2006
 *
 */

/**
 * @author timo
 *
 */
public class XMLSearchWS extends WSTool{
	
    public XMLSearchWS(){
        super();
    }
    
    public void process(){
        //OUTPUT DIRECTORY
        String dir = null;
        Object d = arguments.get("dir");
        if (d==null)
        {
            dir = "." + File.separator;
        }else{
            if (d instanceof ArrayList)
            {
                dir = (String)((ArrayList)d).get(((ArrayList)d).size()-1);
                System.out.println("Exporting to " + dir +".\n Ignoring other -dir tags.");
            }else{
                dir = (String)d;
            }
            if (!dir.endsWith(File.separator))
            {
                dir +=File.separator;
            }
        }

		File f = new File(dir);
		if (!f.exists())
		{
		    f.mkdir();
		}
		
	    int _return = 0;
	    try {
            String service_session = createServiceSession();
            
            String field =(String)arguments.get("field");
            if (field==null)
            {
                System.out.println("ERROR CODE 27: Missing search field.");
                displayHelp();
                System.exit(27);
            }
            
            String comparison = (String)arguments.get("c");
            if (comparison==null){
                comparison ="=";
            }
            
            String value = (String)arguments.get("v");
            if (value==null)
            {
                System.out.println("ERROR CODE 28: Missing search value.");
                displayHelp();
                System.exit(28);
            }
            
            
            String dataType = (String)arguments.get("dataType");
            
            if (dataType==null)
            {
                int index = -1;
                index = field.indexOf('.');
                if (index==-1){
                    index = field.indexOf('/');
                }else{
                    int other = field.indexOf('/');
                    if (other ==-1){
                        
                    }else if (other < index){
                        index=other;
                    }
                }
                
                if (index==-1)
                {
                    System.out.println("ERROR CODE 41: Invalid field format.");
                    displayHelp();
                    System.exit(41);
                }
                dataType = field.substring(0,index);
            }

            Object[] sessions = getIdentifiers(service_session, field, comparison, value, dataType);

    	    if (!quiet)System.out.println(sessions.length + " Matching Item(s) Found.");
    	    
            for (int i =0;i<sessions.length;i++)
            {
                Object id = (Object)sessions[i];
                
                try {
                    String createdFile = execute(host, service_session, id, dataType, dir, quiet);
                    System.out.println(createdFile);
                } catch (SAXException e) {
                    System.out.println("ERROR CODE 30: Invalid XML Received.");
                    System.out.println("This could be do to network instability. Please re-try your request.  If the problem persists contact your IT Management.");
                    _return= 30;
                } catch (ParserConfigurationException e) {
                    logError(e);
                }
            }
            
            closeServiceSession(service_session);
        }catch(AxisFault ex2)
        {
            String fault = ex2.getFaultString();
            if (fault==null){
                System.out.println("ERROR CODE 33: \nWeb Service Exception: " + host + "\n" + ex2.getMessage());
                logError(ex2);
                System.exit(33);
            }else if (fault.indexOf("PasswordAuthenticationException")!=-1)
            {
      		    System.out.println("ERROR CODE 99: \nInvalid Password.");
                logError(ex2);
            	System.exit(99);
            }else if (fault.indexOf("FailedLoginException")!=-1)
            {
      		    System.out.println("ERROR CODE 98: \nFailed Login. Review username and password.");
                logError(ex2);
            	System.exit(98);
            }else if (fault.indexOf("UserNotFoundException")!=-1)
            {
      		    System.out.println("ERROR CODE 97: \nFailed Login. Review username and password.");
                logError(ex2);
            	System.exit(98);
            }else if (fault.indexOf("EnabledException")!=-1)
            {
      		    System.out.println("ERROR CODE 96: \nFailed Login. Account disabled.");
                logError(ex2);
            	System.exit(98);
            }else{
      		    System.out.println("ERROR CODE 32: \nWeb Service Exception @ " + host + "\n" + fault);
                logError(ex2);
            	System.exit(32);
            }
        }catch (RemoteException ex) {
		    System.out.println("ERROR CODE 33: " + ex.getMessage());
            logError(ex);
            _return= 33;
        } catch (ServiceException ex) {
            System.out.println("ERROR CODE: 11\nWeb Service Exception: " + host + "\n" + ex.getMessage());
            logError(ex);
			_return= 11;
        }catch (MalformedURLException e) {
            System.out.println("ERROR CODE: 12\nUnknown URL: " + host);
            logError(e);
            System.exit(12);
        } catch (IOException e) {
            System.out.println("ERROR CODE: 13\nWeb Service Exception @ " + host + "\n" + e.getMessage());
            logError(e);
            System.exit(13);
        }
		System.exit(_return);
    }
    
    /**
     * @param host
     * @param service_session
     * @param id
     * @param dataType
     * @param dir
     * @param quiet
     * @return Path to created XML File
     * @throws FileNotFoundException
     * @throws MalformedURLException
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public String execute(String host, String service_session,Object id, String dataType, String dir, boolean quiet)throws FileNotFoundException, MalformedURLException, IOException, SAXException, ParserConfigurationException{
        int counter = 0;
        String finalName = id + ".xml";
        File outFile = new File(dir,finalName);
        while(outFile.exists())
        {
            finalName = id + "_v" + (counter++) + ".xml";
            outFile = new File(dir,finalName);
        }
        FileOutputStream out = new FileOutputStream(outFile);
        
        writeXMLtoOS(host, service_session, id, dataType, dir, quiet, out);
            
            out.close();
            
            parse(outFile);
        
            return dir + finalName;
    }
    

    
    public static void parse(java.io.File data) throws IOException, SAXException, ParserConfigurationException{
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        
        //get a new instance of parser
        SAXParser sp = spf.newSAXParser();
        //parse the file and also register this class for call backs
        sp.parse(data,new DefaultHandler());

        
        return ;
    }
    
    public void displayHelp(){
        System.out.println("\nXML Search Web Service\n");
        System.out.println("");
        System.out.println("Parameters:");
        System.out.println("-userName          USERNAME");
        System.out.println("-password          PASSWORD");
        System.out.println("-host       URL to XNAT based website.  (i.e. http://localhost/xnat)");
        System.out.println("-field      (REQUIRED) field to search on - Must be specified using dot syntax from the parent element. (i.e. ClinicalAssessment.neuro.CDR.memory");
        System.out.println("-c          (i.e. '=','<','<=','>','>=', or 'LIKE').");
        System.out.println("-v          (REQUIRED) value to search for. ");
        System.out.println("-dataType   Root level data type to return.");
        System.out.println("-dir        Output directory.");
        System.out.println("-quiet      Minimize system output.");
        System.out.println("-h          Print help.");
    }
    
    
    public static void main(String[] args) {
        
        XMLSearchWS xmlSearch = new XMLSearchWS();
        xmlSearch.perform(args);
    }
    
}
