package org.nrg.xnat.client.rest;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.HashMap;

import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;

//Copyright 2007 Washington University School of Medicine All Rights Reserved
/*
 * Created on Sep 28, 2007
 *
 */

public class CloseUserSession extends WSTool {

    
    public void loadHelpText(){
        helpText = new HashMap();
        helpText.put(USER_FLAG, "USERNAME");
        helpText.put(PASSWORD_FLAG, "PASSWORD");
        helpText.put(USER_SESSION_FLAG, "User Session ID to close");
        helpText.put(HOST_FLAG,
                "URL to XNAT based website.  (i.e. http://localhost/xnat).");
        helpText.put(UNZIP_FLAG,
                "Unzip directory VALUES(true,false) (defaults to false).");
        helpText.put(README_FLAG,
                "Whether or not to download the readme file for this session.");
        helpText
                .put(DECOMPRESS_FLAG,
                        "Decompress images. (By default images within the archive are compressed).");;
        helpText.put(PROXY_FLAG, "Proxy server.");
        helpText.put(PROXY_PORT_FLAG, "Proxy server port. (defaults to 80).");
        helpText.put(HELP_FLAG, "Print help.");
        helpText.put(QUIET_FLAG, "Suppress messages.");
    }

    public void displayHelp() {
        System.out.println("\nClose User Web Service Session\n");
        System.out.println("");
        System.out.println("Parameters:");
        printHelpLine(USER_SESSION_FLAG);
        printHelpLine(HOST_FLAG);
        printHelpLine(PROXY_FLAG);
        printHelpLine(PROXY_PORT_FLAG);
        printHelpLine(HELP_FLAG);
        printHelpLine(QUIET_FLAG);
    }

    public static void main(String[] args) {
        CloseUserSession serv = new CloseUserSession();
        serv.perform(args);
    }

    public CloseUserSession() {
        super();
    }

    public void process() {
        
        
        
        try {
            String service_session = (String)arguments.get(USER_SESSION_FLAG);
            closeServiceSession(service_session);
            System.out.println("CLOSED");
        } catch (MalformedURLException e) {
            error(12, "Web Service Exception: " + host + "\n" + e.getMessage(), e);
        } catch (AxisFault ex2) {
            String fault = ex2.getFaultString();
            if (fault == null) {
                error(33, "Web Service Exception: " + host + "\n" + ex2.getMessage(), ex2);
            } else if (fault.indexOf("PasswordAuthenticationException") != -1) {
                error(99, "Invalid Password.", ex2);
            } else if (fault.indexOf("FailedLoginException") != -1) {
                error(98, "Failed Login. Review username and password.", ex2);
            } else if (fault.indexOf("UserNotFoundException") != -1) {
                error(97, "Failed Login. Review username and password.", ex2);
            } else if (fault.indexOf("EnabledException") != -1) {
                error(96, "Failed Login. Account disabled.", ex2);
            } else {
                error(32, "Web Service Exception @ " + host + "\n" + fault, ex2);
            }
        } catch (RemoteException ex) {
            error(33, "Web Service Exception: " + host + "\n" + ex.getMessage(), ex);
        } catch (ServiceException ex) {
            error(11, "Web Service Exception: " + host + "\n" + ex.getMessage(), ex);
        }
    }

}
