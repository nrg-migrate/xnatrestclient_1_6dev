package org.nrg.xnat.client.rest;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.RemoteException;
import java.text.NumberFormat;

import javax.xml.rpc.ServiceException;

//Copyright 2007 Washington University School of Medicine All Rights Reserved
/*
 * Created on Sep 28, 2007
 *
 */

public class GetFileCatalogWS extends WSTool {

    protected static final String MR_FLAG       = "s";
    protected static final String DEST_FLAG       = "f";

    
    public void loadHelpText(){
        super.loadHelpText();
        helpText.put(MR_FLAG, "MR Session ID");
        helpText.put(DEST_FLAG, "File to Output (i.e. './catalog.xml')");
    }

    public void displayHelp() {
        System.out.println("\nRequest File Catalog for MRSession Web Service\n");
        displayCommonHelp();
        printHelpLine(MR_FLAG);
        printHelpLine(DEST_FLAG);
    }

    public static void main(String[] args) {
        GetFileCatalogWS serv = new GetFileCatalogWS();
        serv.perform(args);
    }

    public GetFileCatalogWS() {
        super();
    }

    public void process() {

        if (arguments.get(MR_FLAG) == null) {
            displayHelp();
            System.exit(5);
        }

        if (arguments.get(DEST_FLAG) == null) {
            displayHelp();
            System.exit(6);
        }
        
        try {
            String service_session = this.createServiceSession();
            String mr=(String)arguments.get(MR_FLAG);
            String dest=(String)arguments.get(DEST_FLAG);

            String urlString = host + "app/template/GetFileCatalog.vm/search_element/xnat:mrSessionData/search_field/xnat:mrSessionData.ID/search_value/" + mr;

            try {
                URLConnection url = new URL(urlString).openConnection();
                url.setRequestProperty("Cookie", "JSESSIONID="+service_session);
                // Use Buffered Stream for reading/writing.
                InputStream bis = null;
                BufferedOutputStream bos = null;

                File outFile = new File(dest);
                
                if (outFile.getParentFile()!=null && !outFile.getParentFile().exists()){
                    outFile.getParentFile().mkdirs();
                }
                
                FileOutputStream out = new FileOutputStream(outFile);

                try {
                    bis = url.getInputStream();
                } catch (FileNotFoundException e) {
                    error(39, "File not found on server.  Please review the -s parameter", e);
                }
                
                bos = new BufferedOutputStream(out);

                byte[] buff = new byte[256];
                int bytesRead;
                int loaded = 0;

                java.text.NumberFormat nf = NumberFormat.getInstance();
                while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                    bos.write(buff, 0, bytesRead);
                    bos.flush();
                    loaded = loaded + bytesRead;
                }

                bis.close();
                bos.flush();
                bos.close();
                out.close();
                
                
                if(!quiet)System.out.println(outFile.getAbsolutePath());
            } catch (FileNotFoundException e) {
                error(37, e.getMessage(), e);
            } catch (IOException e) {
                error(38, e.getMessage(), e);
            }
        } catch (MalformedURLException e) {
            error(12, "Web Service Exception: " + host + "\n" + e.getMessage(), e);
        } catch (RemoteException ex) {
            error(33, "Web Service Exception: " + host + "\n" + ex.getMessage(), ex);
        } catch (ServiceException ex) {
            error(11, "Web Service Exception: " + host + "\n" + ex.getMessage(), ex);
        }
    }

}
