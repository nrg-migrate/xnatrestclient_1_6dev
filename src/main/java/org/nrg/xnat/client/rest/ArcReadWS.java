package org.nrg.xnat.client.rest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;
import org.apache.axis.client.Call;

//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Created on Sep 11, 2006
 *
 */

/**
 * @author timo
 *
 */
public class ArcReadWS extends WSTool{
    
    public void process(){
        //OUTPUT DIRECTORY
        String dir = null;
        Object d = arguments.get("o");
        if (d!=null)
        {
            if (d instanceof ArrayList)
            {
                dir = (String)((ArrayList)d).get(((ArrayList)d).size()-1);
                System.out.println("Exporting to " + dir +".\n Ignoring other -o tags.");
            }else{
                dir = (String)d;
            }
            if (!dir.endsWith(File.separator))
            {
                dir +=File.separator;
            }

    		File f = new File(dir);
    		if (!f.exists())
    		{
    		    f.mkdir();
    		}
        }

		
	    int _return = 0;
	    try {
            String service_session = this.createServiceSession();
	        
            String field ="xnat:mrSessionData.ID";
            
            String comparison = "=";
            
//          IDENTIFY SESSIONS
            ArrayList sessions = null;
            Object s=null;
            s = arguments.get("s");
            
            if (s==null && arguments.get("f")==null){
                System.out.println("Missing parameter: -s Session ID");
                displayHelp();
                System.exit(4);
            }
            
            if (s==null)
            {
                s = arguments.get("f");
                if (s instanceof ArrayList){
                    ArrayList files = (ArrayList) s;
                    sessions = new ArrayList();
                    for (int i=0;i<files.size();i++)
                    {
                        s = files.get(i);
                        File sessionFile = new File((String)s);
                        if (sessionFile.exists())
                        {
                            try {
                                sessions.addAll(FileLinesToArrayList(sessionFile));
                            } catch (FileNotFoundException e) {
                                System.out.println("File Not Found: " + s);
                                System.exit(9);
                                return;
                            } catch (IOException e) {
                                System.out.println("Unable to load file: " + s);
                                System.exit(9);
                                return;
                            }

                            if (sessions.size()==0){
                                System.out.println("Unable to load session ids from file: " + s);
                                System.exit(9);
                                return;
                            }
                        }else{
                            System.out.println("Unable to access file: " + s);
                            System.exit(9);
                            return;
                        }
                    }
                }else{
                    sessions = new ArrayList();
                    File sessionFile = new File((String)s);
                    if (sessionFile.exists())
                    {
                        try {
                            sessions = FileLinesToArrayList(sessionFile);
                        } catch (FileNotFoundException e) {
                            System.out.println("File Not Found: " + s);
                            System.exit(9);
                            return;
                        } catch (IOException e) {
                            System.out.println("Unable to load file: " + s);
                            System.exit(9);
                            return;
                        }

                        if (sessions.size()==0){
                            System.out.println("Unable to load session ids from file: " + s);
                            System.exit(9);
                            return;
                        }
                    }else{
                        System.out.println("Unable to access file: " + s);
                        System.exit(9);
                        return;
                    }
                }
            }else{
                if (s instanceof ArrayList){
                    sessions = (ArrayList) s;
                }else{
                    sessions = new ArrayList();
                    sessions.add(s);
                }
            }
            
            
            String dataType = "xnat:mrSessionData";
            


            
         for (int i=0;i<sessions.size();i++)
         {
            String session_id = (String)sessions.get(i);

            URL url = new URL(host + "axis/GetIdentifiers.jws");
            Call call = createCall(service_session);
            call.setTargetEndpointAddress(url);
                
            call.setOperationName("search");
               Object[] params=new Object[]{service_session,field,comparison,session_id,dataType};

            Object[] o = (Object[])call.invoke(params);
    	    
            for (int j =0;j<o.length;j++)
            {
                Object id = (Object)o[j];
                            	
            	call = createCall(service_session);
                url = new URL(host + "axis/VelocitySearch.jws");
                call.setTargetEndpointAddress(url);
                call.setOperationName("search");
                params = new Object[]{service_session,"xnat:mrSessionData.ID","=",id,"xnat:mrSessionData"};
                
                    String txt = (String)call.invoke(params);
                     if (dir ==null)
                     {
                         System.out.println(txt);
                     }else{
                        int counter =0;
                        File outFile = new File(dir + id + ".txt");
                        while (outFile.exists())
                        {
                            outFile = new File(dir + id+"_" + counter++ + ".txt");
                        }
                        
                        if (txt.indexOf("\r\n")==-1){
                            txt = txt.replaceAll("\n", System.getProperty("line.separator")); 
                        }
                        
                        OutputToFile(txt,outFile.getAbsolutePath());
                        System.out.println("Created file: " + outFile.getAbsolutePath());
                     }
            }
        }
            
            closeServiceSession(service_session);
        }catch(AxisFault ex2)
        {
            String fault = ex2.getFaultString();
            if (fault==null){
                System.out.println("ERROR CODE 33: \nWeb Service Exception: " + host + "\n" + ex2.getMessage());
                logError(ex2);
                System.exit(33);
            }else if (fault.indexOf("PasswordAuthenticationException")!=-1)
            {
      		    System.out.println("ERROR CODE 99: \nInvalid Password.");
                logError(ex2);
            	System.exit(99);
            }else if (fault.indexOf("FailedLoginException")!=-1)
            {
      		    System.out.println("ERROR CODE 98: \nFailed Login. Review username and password.");
                logError(ex2);
            	System.exit(98);
            }else if (fault.indexOf("UserNotFoundException")!=-1)
            {
      		    System.out.println("ERROR CODE 97: \nFailed Login. Review username and password.");
                logError(ex2);
            	System.exit(98);
            }else if (fault.indexOf("EnabledException")!=-1)
            {
      		    System.out.println("ERROR CODE 96: \nFailed Login. Account disabled.");
                logError(ex2);
            	System.exit(98);
            }else{
      		    System.out.println("ERROR CODE 32: \nWeb Service Exception @ " + host + "\n" + fault);
                logError(ex2);
            	System.exit(32);
            }
        }catch (RemoteException ex) {
		    System.out.println("ERROR CODE 33: " + ex.getMessage());
            logError(ex);
            System.exit(33);
        } catch (ServiceException ex) {
            System.out.println("ERROR CODE: 11\nWeb Service Exception: " + host + "\n" + ex.getMessage());
            logError(ex);
			_return= 11;
        }catch (MalformedURLException e) {
            System.out.println("ERROR CODE: 12\nUnknown URL: " + host);
            logError(e);
            System.exit(12);
        }
		System.exit(_return);
    }

	public static void OutputToFile(String content, String filePath)
	{
		File _outFile=null;
		FileOutputStream _outFileStream=null;
		PrintWriter _outPrintWriter=null;

		_outFile = new File(filePath);
		
		try 
		{
			_outFileStream = new FileOutputStream ( _outFile );
		}  // end try
		catch ( IOException except ) 
		{
			System.out.println(except.getMessage());
		}  
		// Instantiate and chain the PrintWriter
		_outPrintWriter = new PrintWriter ( _outFileStream );
		  
		_outPrintWriter.println(content);
		_outPrintWriter.flush();
		  
		_outPrintWriter.close();
		
		try
		{
			_outFileStream.close();
		}
		catch ( IOException except )
		{
			System.out.println(except.getMessage());
		}
	}
	
    public ArcReadWS(){
        super();
    }
    
    public void displayHelp(){
        System.out.println("\nArc-Read Web Service\n");
        System.out.println("");
        System.out.println("Parameters:");
        System.out.println("-u          USERNAME");
        System.out.println("-p          PASSWORD");
        System.out.println("-host       URL to XNAT based website.  (i.e. http://localhost/xnat)");
        System.out.println("-quiet      Minimize system output.");
        System.out.println("-h          Print help.");
        System.out.println("-s          Session id of the desired session(s).  For multiple sessions, use multiple -s tags.");
        System.out.println("-o          Directory to write output to.");
    }
    
    
    public static void main(String[] args) {
        
        ArcReadWS arcRead = new ArcReadWS();
        arcRead.perform(args);
    }
    
}