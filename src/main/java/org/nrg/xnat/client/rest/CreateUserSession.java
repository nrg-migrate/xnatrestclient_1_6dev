package org.nrg.xnat.client.rest;

import java.net.MalformedURLException;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;

//Copyright 2007 Washington University School of Medicine All Rights Reserved
/*
 * Created on Sep 28, 2007
 *
 */

public class CreateUserSession extends WSTool {

    public void loadHelpText(){
        super.loadHelpText();
    }

    public void displayHelp() {
        System.out.println("\nCreate User Web Service Session\n");
        System.out.println("");
        System.out.println("Parameters:");
        printHelpLine(USER_FLAG);
        printHelpLine(PASSWORD_FLAG);
        printHelpLine(HOST_FLAG);
        printHelpLine(PROXY_FLAG);
        printHelpLine(PROXY_PORT_FLAG);
        printHelpLine(HELP_FLAG);
        printHelpLine(QUIET_FLAG);
    }

    public static void main(String[] args) {
        CreateUserSession serv = new CreateUserSession();
        serv.perform(args);
    }

    public CreateUserSession() {
        super();
    }

    public void process() {
        
        try {
            String service_session = createServiceSession();
            System.out.println(service_session);
        } catch (MalformedURLException e) {
            error(12, "Web Service Exception: " + host + "\n" + e.getMessage(), e);
        } catch (AxisFault ex2) {
            String fault = ex2.getFaultString();
            if (fault == null) {
                error(33, "Web Service Exception: " + host + "\n" + ex2.getMessage(), ex2);
            } else if (fault.indexOf("PasswordAuthenticationException") != -1) {
                error(99, "Invalid Password.", ex2);
            } else if (fault.indexOf("FailedLoginException") != -1) {
                error(98, "Failed Login. Review username and password.", ex2);
            } else if (fault.indexOf("UserNotFoundException") != -1) {
                error(97, "Failed Login. Review username and password.", ex2);
            } else if (fault.indexOf("EnabledException") != -1) {
                error(96, "Failed Login. Account disabled.", ex2);
            } else {
                error(32, "Web Service Exception @ " + host + "\n" + fault, ex2);
            }
        } catch (RemoteException ex) {
            error(33, "Web Service Exception: " + host + "\n" + ex.getMessage(), ex);
        } catch (ServiceException ex) {
            error(11, "Web Service Exception: " + host + "\n" + ex.getMessage(), ex);
        }
    }
}
