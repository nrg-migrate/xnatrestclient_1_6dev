A classpath issue with org.nrg.xnat.client.rest.XNATRestClient surfaced where it could not find org.apache.commons.codec.DecoderException. Couldn't figure it out at first.

This repo was created when it looked like we would have to refactor org.nrg.xnat.client.rest.XNATRestClient to use Apache HttpComponents HttpClient in place of the deprecated Apache Commons HttpClient.

This turned out to be unnecessary; the issue was that the classpath was hardcoded in the MANIFEST in the JAR file, and it was only looking for version commons-codec-1.2.jar.  Updated this to our most recent version (1.5), and all is well.
 